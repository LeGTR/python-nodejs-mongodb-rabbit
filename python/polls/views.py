from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
import json

from kombu import Connection, Producer, Exchange, Queue

from django.shortcuts import redirect, render


def index(request):
    return HttpResponse("Hello World, вы в polls")


def detail(request, question_id):
    return HttpResponse("Вы просматриваете вопрос %s." % question_id)


def results(request, question_id):
    response = "Перед вами результаты вопроса %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("Вы голосуете за вопрос %s." % question_id)



def amqp(request):

    with Connection(settings.AMQP) as conn:
        #producer = Producer(conn)
        channel = conn.channel()
        exchange = Exchange("callback1.signed_up", type="direct")
        producer = Producer(exchange=exchange, channel=channel, routing_key="callback_1")
        queue = Queue(name="callback1.callback_1", exchange=exchange, routing_key="callback_1")
        queue.maybe_bind(conn)
        queue.declare()
        data = {
            "callback_url": "http://keltest.ru/pay/callback3.php",
            "request_body": {
                "tx_id": "1234tttt",
                "payment_id": "1111",
                "status": "deposited",
                "extra": {
                "ls": 12345
                },
                "result": True
            },
            "payment_id": 1234
        }
        json_string = json.dumps(data)
        producer.publish(json_string)


    return HttpResponse("amqp отработало!")