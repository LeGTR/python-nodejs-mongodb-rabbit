#!/bin/bash

# @note: remove all aliases by security reasons
# unalias -a

# @note: trace to stderr each bash command (variable(s) are/is expanded before execution)
# set -x

db_name=nodejs
master_table=operation_log
# @note: period can be changed in any time (day(s), week(s), month(s) and year(s))
new_period="1 week"
# @note: drop old tables (1) or not (0)
delete_old=1
no_delete_last="6 months"
curr_date=`/bin/date +%F`

LOG=/var/log/partitioning.log
SQL=/tmp/partitioning-`/bin/date -d "${curr_date}" +%Y%m%d`.sql
DUMP_DIRECTORY=/tlogbackup

for arg in "$@"
do
    case $arg in
        -n|--no-delete)
        delete_old=0
        ;;
        *)
        # unknown option, ignore
        ;;
    esac
done

# @note: select child tables and check constraint definitions for each of them (ordered
#   by check constaint definition, it is more reliable way)
OIFS="${IFS}"; IFS=$'\n'
childs_info=($(echo "
    SELECT relname,
           SUBSTRING(pg_constraint.consrc FROM '(\d{4}-\d{2}-\d{2}).*AND.*'),
           SUBSTRING(pg_constraint.consrc FROM '.*AND.*(\d{4}-\d{2}-\d{2}).*')
    FROM pg_inherits, pg_class, pg_constraint
    WHERE
        pg_class.oid = pg_inherits.inhrelid
        AND pg_class.oid = pg_constraint.conrelid
        AND inhparent IN (SELECT oid FROM pg_class WHERE relname='${master_table}')
    ORDER BY pg_constraint.consrc" |
        sudo -u postgres psql ${db_name} -t -A -F $'\t'))
IFS="${OIFS}"

# @note: all columns without "id"
columns=$(echo "
    SELECT column_name
    FROM information_schema.columns
    WHERE table_name = '${master_table}' AND table_schema = 'public' AND column_name != 'id'
    ORDER BY ordinal_position" |
        sudo -u postgres psql ${db_name} -t -A -R ',')

prev_child_info="${childs_info[-1]}"
prev_child=$(echo "${prev_child_info}" | cut -f 1 | tr -d ' ')
prev_start_date=$(echo "${prev_child_info}" | cut -f 2 | tr -d ' ')
prev_end_date=$(echo "${prev_child_info}" | cut -f 3 | tr -d ' ')
unset prev_child_info

new_start_date="${prev_end_date}"
new_end_date=`/bin/date -d "${prev_end_date} + ${new_period}" +%F`
new_child=${master_table}`/bin/date -d "${new_start_date}" +y%Y_m%m_d%d`

# @note: check new_start_date vs curr_date (not necessary if ts surely > 00:00)
if [ "${prev_end_date}" \> "${curr_date}" ]; then
    echo "too early: current date ${curr_date} still in previous child (< ${prev_end_date})"
    exit 1
fi

# @todo: check table existance and exit if is true

# @note: old childs to dump & drop
drop_childs=""
if [ "${delete_old}" == "1" ]; then
    delete_before=`/bin/date -d "${curr_date} - ${no_delete_last}" +%F`
    for info in "${childs_info[@]}"; do
        child=`echo "${info}" | cut -f 1 | tr -d ' '`
        end_date=`echo "${info}" | cut -f 3 | tr -d ' '`
        if [ ! -z "${end_date}" -a "${delete_before}" \> "${end_date}" ]; then
            drop_childs="${drop_childs} ${child}"
        fi
    done
fi

truncate -s 0 $SQL

# @note: create a new partition with CHECK
echo "CREATE TABLE ${new_child} (CHECK (ts >= DATE '${new_start_date}' AND ts < DATE '${new_end_date}'))
    INHERITS (${master_table});" >> $SQL

# @note: grant privilegies to select, insert, update and delete for user logger on new partition
echo "GRANT SELECT, INSERT ON ${new_child} to logger;" >> $SQL

echo "CREATE INDEX ON ${new_child} (tr_num, mp_id);" >> $SQL

# @note: change the trigger function to write in new partition (complex variant),
#   use this insteaf of simple variant if you're not sure that ts is later then 00:00
###echo "CREATE OR REPLACE FUNCTION log_insert_proc() RETURNS trigger AS \$lol\$
###    BEGIN
###        IF NEW.ts >= '${new_start_date}'::DATE AND NEW.ts < '${new_end_date}'::DATE THEN
###            -- new child table
###            INSERT INTO ${new_child} VALUES (NEW.*);
###        ELSIF NEW.ts >= '${prev_start_date}'::DATE AND NEW.ts < '${prev_end_date}'::DATE THEN
###            -- previous child table
###            INSERT INTO ${prev_child} VALUES (NEW.*);
###        ELSE
###            -- leave row in the master table
###            RETURN NEW;
###        END IF;
###        RETURN NULL;
###    END;
###    \$lol\$ LANGUAGE plpgsql;
###" >> $SQL

# @note: change the trigger function to write in the new partition (simple variant),
#   use this instead of complex variant when ts is surely after 00:00
echo "CREATE OR REPLACE FUNCTION log_insert_proc() RETURNS trigger AS \$lol\$
    BEGIN
        IF NEW.ts < '${new_end_date}'::DATE THEN
            -- new child table
            INSERT INTO ${new_child} VALUES (NEW.*);
        ELSE
            -- leave row in the master table
            RETURN NEW;
        END IF;
        RETURN NULL;
    END;
    \$lol\$ LANGUAGE plpgsql;
" >> $SQL

echo "COMMENT ON FUNCTION log_insert_proc() IS
    'Used for \"redirect\" rows insertion from the master table to a child (partition) table.';" >> $SQL

echo "ALTER FUNCTION log_insert_proc() OWNER TO logger;" >> $SQL

# @note: move the rows from master to the new partition
echo "INSERT INTO ${new_child} (${columns}) SELECT ${columns} FROM ONLY ${master_table}
    WHERE ts >= '${new_start_date}'::DATE AND ts < '${new_end_date}'::DATE;" >> $SQL
echo "DELETE FROM ONLY ${master_table}
    WHERE ts >= '${new_start_date}'::DATE AND ts < '${new_end_date}'::DATE;" >> $SQL

# @note: dump & drop old partitions
if [ ! -z "${drop_childs}" ]; then
    for child in ${drop_childs}; do
        echo "dump table ${child} ..."
        # @warn: dump with link to parent table
        sudo -u postgres pg_dump --quote-all-identifiers --table=${child} ${db_name} | gzip -9 > "${DUMP_DIRECTORY}/${child}.pg.gz"
        if [ "${?}" -eq "0" ]; then
            echo "DROP TABLE ${child};" >> $SQL
        else
            echo "do not drop table ${child} - incorrect return code from pg_dump"
        fi
    done
fi

cat $SQL | sudo -u postgres psql ${db_name} >> $LOG
