#!/bin/sh

set -e
cd /TLOG/deps/
md5sum -c < md5sums
pip install -I \
    setuptools-18.4.tar.gz wheel-0.26.0.tar.gz \
    simplejson-3.8.0.tar.gz \
    tornado-4.2.1.tar.gz Momoko-2.2.1.tar.gz