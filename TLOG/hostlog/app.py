import os
import sys
import logging
from datetime import datetime, timedelta

import momoko
import psycopg2.extras
import tornado.ioloop
import tornado.httpserver
import tornado.web
from tornado.options import define, options, parse_command_line, parse_config_file

from handlers import *


define('port', default=8001, help='run on the given port', type=int)
define('dsn', type=str, help='Database source name',
       default='dbname=nodejs user=nodejs password=nodejs host=db',
       metavar='DSN')
define('config', type=str, help='path to config file',
       callback=lambda path: parse_config_file(path, final=False),
       metavar='PATH')


class Application(tornado.web.Application):

    def __init__(self, settings):
        handlers = [
            (r"/log/test", LogSaveHandlerTest),
            (r"/log/save", LogSaveHandler),
            (r"/log/get", LogGetHandler),
        ]

        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    base_path = os.path.realpath(os.path.dirname(sys.argv[0]))

    parse_command_line()
    settings = dict(static_path=base_path,
                    template_path=base_path,
                    cookie_secret='asjdkljcklasjdkjaksdjvasdfv',
                    xsrf_cookies=False,
    )

    app = Application(settings)

    ioloop = tornado.ioloop.IOLoop.instance()
    app.db = momoko.Pool(
        dsn=options.dsn,
        size=2,
        max_size=6,
        ioloop=ioloop,
        reconnect_interval=500,
        auto_shrink=True,
        shrink_delay=timedelta(minutes=10),
        shrink_period=timedelta(minutes=10),
        cursor_factory=psycopg2.extras.RealDictCursor,
    )

    # @note: connect pool
    future = app.db.connect()
    ioloop.add_future(future, lambda f: ioloop.stop())
    ioloop.start()
    # @note: raises exception on connection error
    future.result()

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port, address='127.0.0.1')
    print('127.0.0.1')
    print(options.port)
    ioloop.start()
