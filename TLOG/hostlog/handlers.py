import logging
from datetime import datetime
import urllib.parse

import simplejson as json
import tornado.web
import tornado.gen


__all__ = ['LogSaveHandler', 'LogGetHandler', 'LogSaveHandlerTest']

log = logging.getLogger('application')
SOURCES = {
    '1': 'Paymo',
    '2': 'PayNode',
    '3': 'AntiFraud',
    '4': 'Checkout',
    '5': 'AMQP SERVICE'
}


def ts_adapter(obj):
    return obj.strftime('%Y-%m-%d %H:%M:%S') if isinstance(obj, datetime) else repr(obj).replace('\n', '\\\\n')


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

class LogSaveHandlerTest(BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        print("test")
    def get(self):
        print("test")

class LogSaveHandler(BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        params = self._get_params()
        print(params)
        if not params:
            raise tornado.web.HTTPError(400)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            sql = """
                INSERT INTO operation_log (source_id, mp_id, payment_id, tr_num, ts, message)
                        VALUES (%s, %s, %s, %s, %s, %s);
            """
            yield connection.execute(sql.strip(), params)

        self.finish()


    def _get_params(self):
        source_id = self.get_argument('source_id', None)
        tr_num = self.get_argument('tr_num', None)
        ts = self.get_argument('ts', None)
        msg = self.get_argument('message', None)
        mp_id = self.get_argument('mp_id', None)
        payment_id = self.get_argument('payment_id', None)

        if not source_id or source_id not in SOURCES:
            log.error('no or invalid "source_id"')
            return
        elif not ts:
            log.error('no "ts"')
            return
        elif not msg or not msg.strip():
            log.error('no "messsage"')
            return
        elif mp_id and not mp_id.isdigit():
            log.warning('merchant point id should be an integer')
            mp_id = None
        elif payment_id and not payment_id.isdigit():
            log.warning('payment id should be an integer')
            payment_id = None

        try:
            datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            log.error('invalid "ts"')
            return

        return (
            int(source_id), int(mp_id) if mp_id else None,
            int(payment_id) if payment_id else None,
            tr_num or None, ts, msg.strip(),
        )


class LogGetHandler(BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        params = self._get_params()
        if not params:
            raise tornado.web.HTTPError(400)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            sql = "SELECT * FROM operation_log"
            args = tuple()
            if params[0]:
                sql += " WHERE tr_num = %s"
                args += (params[0],)
                if params[1]:
                    sql += " AND mp_id = %s"
                    args += (params[1],)
            elif params[2] and params[3]:
                sql += " WHERE ts BETWEEN %s AND %s"
                args += (params[2], params[3],)

            sql += " ORDER BY id"

            cursor = yield connection.execute(sql.strip(), args)
            self.set_header('Content-Type', 'application/json')
            a = json.dumps(cursor.fetchall(), default=ts_adapter)
            print(a)
            self.write(a)
            cursor.close()

        self.finish()

    def _get_params(self):
        # @note: merchant transaction number
        tr_num = self.get_argument('tr_num', None)
        # @note: merchant point id
        mp_id = self.get_argument('mp_id', None)
        from_ = self.get_argument('from', None)
        to_ = self.get_argument('to', None)

        if mp_id and not mp_id.isdigit():
            log.error('invalid mp_id')
            return

        if not tr_num and (not from_ or not to_):
            log.error('no "tr_num", "from" and "to"')
            return

        if from_:
            try:
                datetime.strptime(from_, '%Y-%m-%d')
            except ValueError:
                log.error('invalid "from"')
                return

        if to_:
            try:
                datetime.strptime(to_, '%Y-%m-%d')
            except ValueError:
                log.error('invalid "to"')
                return

        return (tr_num, mp_id, from_, to_)
