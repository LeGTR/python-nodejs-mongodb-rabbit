
module.exports = class CallbackModel{
 
    constructor(
        db = null,
        callback_id = ''
    ){
        this.db = db;
        this.callback_id = callback_id;
        this.table = "callback_errors";
        this.collection = null;
    }

    async save( msg ){
        console.log(" --- SAVE IN MONGODB --- ");
        try {
            console.log(" --- try IN MONGODB --- ");
            const client = this.db.connect(
                async (err, client) => {
                    if (err) {
                        console.log('Connection error: ', err)
                        throw err
                    }
                    console.log('Connected')
                    const db = client.db('nodejs').collection(this.table).insertOne(msg, (err, results) => {
                        console.log(err);
                        console.log(results);
                        client.close()
                    })
                    
                }
            );
            
        }catch(err) {
            console.log(" --- catch IN MONGODB --- ");
            console.log(err);
        } finally {
            console.log(" --- finally IN MONGODB --- ");
            //await this.db.close();
        }

    }
    
}

