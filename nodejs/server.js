// Import packages and set the port

const port = 3002;

const AmpqController = require("./controller/amqp.controller.js");
const ApiController = require("./controller/api.controller.js");
const _sendMessage = require("./controller/sendMessage.controller.js");
const CallbackController = require("./controller/Callback.controller.js");
const CallbackModel = require('./models/Callback.model');
const tlogController = require("./controller/tlog.controller.js");

const mongodb = require("./db/mongo");

/*
const mongo = require('mongodb').MongoClient
mongo.connect(
	'mongodb://nodejs:nodejs@mongo:27017/',
	(err, client) => {
		if (err) {
			console.log('Connection error: ', err)
			throw err
		}
		console.log('Connected')
		try {
			const db = client.db('db_name')
			var users = db.createCollection('users')
			users = db.collection('users');
			users.insertOne(
				{ id: 1, login: 'login1', name: 'name1', gender: 'male' },
				(err, result) => {
					if (err) {
						console.log('Unable insert user: ', err)
						throw err
					}
				}
			)
        }catch(err) {
            console.log(" --- catch IN MONGODB --- ");
            console.log(err);
        } finally {
            console.log(" --- finally IN MONGODB --- ");
			//client.close()
            //await this.db.close();
        }


	}
)

return false;
*/

const Ampq = new AmpqController( connect_header = "heartbeat=60", prefetch = 10, queue = 'callback1.callback_1', durable = true, 
									sendMessage = new _sendMessage(),
									callback = new ApiController(
										connect_header = 'heartbeat=60',
										prefetch = 10,
										queue = 'callback2.callback_2',
										durable = true,
										routingKey = 'callback_2',
										exchange = 'callback2.signed_up'
									),
									logger = new tlogController()
								);
Ampq.start();

const Ampq2 = new AmpqController( connect_header = "heartbeat=60", prefetch = 10,	queue = 'callback2.callback_2', durable = true,
											sendMessage = new _sendMessage(),
											callback = new CallbackController( model = new CallbackModel( mongodb ) ),
											logger = new tlogController()
 								);
Ampq2.start();


const express = require("express");
const app = express();
app.use(express.json());

// Start the server
const server = app.listen(port, (error) => {
	if (error) return console.log(`Error: ${error}`);
	console.log(`Server listening on port ${server.address().port}`);
});