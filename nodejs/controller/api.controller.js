

module.exports = class ApiController {

    amqplib = require('amqplib');
    amqpUrl = process.env.AMQP_URL || 'amqp://localhost:5673';

    constructor(
        connect_header = "heartbeat=60",
        prefetch = 10,
        queue = 'user.sign_up_email',
        durable = true,
        routingKey = 'sign_up_email',
        exchange = 'user.signed_up'

    ){
        this.connect_header = connect_header;
        this.prefetch = prefetch;
        this.queue = queue;
        this.durable = durable;
        this.routingKey = routingKey;
        this.exchange = exchange;
    }

    async send(msg){
        const connection = await this.amqplib.connect(this.amqpUrl, this.connect_header);
        const channel = await connection.createChannel();

        console.log('Publishing');
        
        await channel.assertExchange(this.exchange, 'direct', {durable: this.durable});
        await channel.assertQueue(this.queue, {durable: this.durable});
        await channel.bindQueue(this.queue, this.exchange, this.routingKey);
        
        await channel.publish(this.exchange, this.routingKey, Buffer.from(JSON.stringify(msg)));
        console.log('Message published');
    }
}
