const { createLogger, format, transports } = require('winston');
const axios = require('axios');


module.exports = class tlogController {
    request = require('request');
    constructor(
        
    ){
        this.result = false;
        this.logging = ''
        this.logger  = createLogger({
          format: format.combine(
            format.json()
          ),
          transports: [new transports.Console()]
        });
    }



    send ( callback_url = '', msg = {} ) {

      try {
        
        this.logger.log({
          level: 'debug',
          status: 'success',
          message: msg
        });
        /*
        axios.post('http://127.0.0.1:8001/log/save', msg)
        .then(function (response) {
          console.log(response);
          this.logger.log({
            level: 'debug',
            status: 'success',
            message: msg
          });
        })
        .catch(function (error) {
          console.log(error);
          this.logger.error(error);
        });
        */
      } catch (e) {
        this.logger.error(e);
      }

    }

}
