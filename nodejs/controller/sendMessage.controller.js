

module.exports = class sendMessageController {
    request = require('request');
    constructor(){
        this.result = false;

    }

    send ( callback_url = '', msg = {} ) {
        var requestData = {
            request: {
              slice: [
                {
                  origin: "ZRH",
                  destination: "DUS",
                  date: "2014-12-02"
                }
              ],
              passengers: {
                adultCount: 1,
                infantInLapCount: 0,
                infantInSeatCount: 0,
                childCount: 0,
                seniorCount: 0
              },
              solutions: 2,
              refundable: false
            }
        };
        return new Promise((resolve, reject) => {
            this.request(callback_url,
                { json: true, body: msg },
                function(error, response, body) {
                    if (!error && response.statusCode === 200) {
                        console.log(' ----- result sendMessage SUCCESSFULL ------');
                        console.log(body)
                        if ( body.result == true ) {
                            this.result = true;
                            resolve(this.result);
                        }else{
                            reject(false);
                        }
                    }
                    else {
                        console.log("error: " + error)
                        console.log(response)
                        //console.log("response.statusCode: " + response.statusCode)
                        //console.log("response.statusText: " + response.statusText)
                        this.result = false;
                        reject(new Error(error));
                    }
            });
        });
        
    }

}
