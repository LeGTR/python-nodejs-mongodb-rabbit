module.exports = class AmpqController {

    amqplib = require('amqplib');
    amqpUrl = process.env.AMQP_URL || 'amqp://localhost:5673';

    constructor(
        connect_header = "heartbeat=60",
        prefetch = 10,
        queue = 'user.sign_up_email',
        durable = true,
        sendMessage = null,
        callback = null,
        logger = null
    ){
        this.connect_header = connect_header;
        this.prefetch = prefetch;
        this.queue = queue;
        this.durable = durable;
        this.sendMessage = sendMessage;
        this.callback = callback;
        this.logger = logger;
    }

    async start () { 
        const connection = await this.amqplib.connect(this.amqpUrl, this.connect_header);
        const channel = await connection.createChannel();
        channel.prefetch( this.prefetch );

        process.once('SIGINT', async () => { 
        console.log('got sigint, closing connection');
            await channel.close();
            await connection.close(); 
            process.exit(0);
        });

        await channel.assertQueue( this.queue, {durable: durable});
        await channel.consume(this.queue, async (msg) => {
            console.log('processing messages');
            await this.processMessage(msg);
            await channel.ack(msg);
        }, 
        {
            noAck: false,
            consumerTag: 'email_consumer'
        });

        console.log(" [*] " + this.queue);
        console.log(" [**] Waiting for messages. To exit press CTRL+C");
    }

    
    async processMessage(msg) {
        console.log(" [*] " + this.queue);
        console.log('Call email API here');
        console.log(msg.content.toString());
        this.logger.send(msg.content.toString());
        //call your email service here to send the email
        var _msg = JSON.parse(msg.content.toString());
        var res = this.even_or_odd( this.getRandomArbitrary() );

        /** Отправка callback по указанному адресу */
        if ( this.sendMessage ) {
            console.log('sendMessage for url ' + _msg.callback_url);
            /** в зависимости от результат отправляем msg на переработку */
            this.sendMessage.send(_msg.callback_url , _msg  ).then(result => {
                /** При успешной обработке */
                console.log('THEN если успешно прошло');
                
                console.log(result);
                var message_result = true;
                console.log(message_result);
                this.logger.send(result);

            }).catch(err => {
                /** При не успешной обработке */
                console.log('CATCH если ошибка');
                console.log(err);
                this.logger.send(err);
                
                console.log(" [*] " + this.queue);
                console.log(" [***] Errors. Replay callback 2");
                this.callback.send( _msg );
                if ( this.callback.queue ) {
                    console.log(" [***] Send for " + this.callback.queue);
                }

            });

            /** в зависимости от результат отправляем msg на переработку Пока установлен рандом */
            /** Можно включить рандомайзер для случайных отправлений */
            /*
            if ( this.callback && res === false ) {
                console.log(" [*] " + this.queue);
                console.log(" [***] Errors. Replay callback 2");
                await this.callback.send( _msg );
                if ( this.callback.queue ) {
                    console.log(" [***] Send for " + this.callback.queue);
                }
            }
            */
        }
        console.log(" [****] END WORK PROCESS MESSAGE");
        console.log(" \n ");
    }

    getRandomArbitrary(min = 1, max = 100) {
        var res = Math.floor(Math.random() * (max - min) + min);
        return res;
    }
    even_or_odd(number) {
        return number % 2 === 0 ? true : false
    }
}
