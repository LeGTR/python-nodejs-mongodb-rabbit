const users = [
	{
		id: 1,
		name: "Richard Hendricks",
		email: "richard@piedpiper.com",
	},
	{
		id: 2,
		name: "Bertram Gilfoyle",
		email: "gilfoyle@piedpiper.com",
	},
];

const fs = require('fs'); 

const mysql_sql = fs.readFileSync("sql/mysql.sql", "utf8"); 
const pg_sql = fs.readFileSync("sql/postgresssql.sql", "utf8"); 
mysql = require("../db/mysql.js");
pg = require("../db/pg.js");

const Router = require("express");
const router = new Router();
const apiController = require('../controller/api.controller');
const callbackModel = require('../models/Callback.model');



router.get("/", (request, response) => {
    response.send({
        message: "Node.js and Express REST API",
    });
});
router.get("/test", (request, response) => {
    response.send(users);
});

router.get("/migrate", (request, response) => {
	mysql.query( mysql_sql );
	pg.query( pg_sql );
    response.send({
		message: "migrate successfull",
	});
});

router.get("/callback", (request, response) => {
	callback = '{"key": "value"}';
	type = 'first';
	attempt = '1';
	status = '1';

	callback_model = new callbackModel(
		callback_id = '',
		callback,
		type,
		attempt,
		status
	);
	callback_model.save();
    response.send({
        message: "callback save",
    });
});

router.get("/add", apiController.add);
router.post("/ampq", apiController.ampq_test);



module.exports = router