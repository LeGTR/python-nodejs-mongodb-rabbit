
const mongo = require('mongodb').MongoClient;
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://nodejs:nodejs@mongo:27017';
const client = new mongo(url);


module.exports = client;